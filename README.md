

# How to build
## For 1.12.0 and Newer

#### 1. Create a workspace

     mkdir -p autoware.ai/src
     cd autoware.ai


#### 2. Download the workspace configuration for Autoware.AI.
 For the 1.13.0 release:

     wget -O autoware.ai.repos "https://gitlab.com/autocore_publish/autoware.ai/repo/raw/autocore-dev-1.13/autoware.ai.repos?inline=false"


#### 3. Download Autoware.AI into the workspace.
     vcs import src < autoware.ai.repos

#### 4. Install dependencies using rosdep.
     rosdep update
     rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO


#### 5. Compile the workspace
With CUDA support

    $AUTOWARE_COMPILE_WITH_CUDA=1 colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release

Without CUDA Support

    colcon build --cmake-args -DCMAKE_BUILD_TYPE=Release



## For 1.11.1 and Older


 #### 1. Clone the GitHub repository.

     cd $HOME
     git clone https://gitee.com/autowarefoundation/autoware.git
     cd autoware


 #### 2. Check out the tag for the appropriate version. To see a list of versions, type git tag.

    $ git checkout 1.11.1
    $ cd ros


 #### 3. Install dependencies using rosdep.

    rosdep update
    rosdep install -y --from-paths src --ignore-src --rosdistro $ROS_DISTRO

#### 4. Compile the workspace
Version 1.11.0 or 1.11.1 Compile using colcon

    ./colcon_release

Version 1.10 or older Compile using catkin

    ./catkin_make_release


